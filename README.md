# Geometry-driven topological defects in a polar system

## Table of content
- [Overview](#overview)
- [System requirements](#system-requirements)
  - [Hardware requirements](##hardware-requirements)
  - [Software requirements](##software-requirements)
      - [OS requirements](###os-requirements)
      - [Python dependencies](###python-dependencies)
- [Installation guide](#installation-guide)
- [Instructions for use](#instructions-for-use)
  - [Repo content](#repo-content)
- [License](#license)

## Overview

This repository contains all the code related to [Guruciaga et al., "Boundary geometry controls a topological defect transition that determines lumen nucleation in embryonic development"](https://arxiv.org/abs/2403.08710).

## System requirements

### Hardware requirements

This package requires only a standard computer with enough RAM to support the in-memory operations.

### Software requirements

#### OS requirements

The package has been tested on MacOS Sonoma (14.6.1).

#### Python dependencies

This package depends on the [FEniCSx](https://github.com/FEniCS) platform for solving partial differencial equations (PDEs) with the finite-element method (FEM), implemented in the package `dolfinx`. It also requires `ufl` and `gmsh`, as well as standard python packages like `numpy`, `scipy`, `os`, `statistics` and `random`. `matplotlib` and `pyvista` are used for visualisation purposes.

## Installation guide

To install from Gitlab, open a Terminal and type `git clone https://git.embl.de/guruciag/geometry-driven-defects`. It should take less than 1 second. Instructions for installing the `dolfinx` package are available [here](https://github.com/FEniCS/dolfinx).

## Instructions for use

The code is written in Jupyter notebooks. Cells must be run in order to reproduce all the figures of the manuscript. One individual run of the `mfd.FEM` function, which implements the FEM to solve the system of PDEs, takes approximately 10 seconds in a normal desktop computer.

### Repo content

- `_my_function_definitions.ipynb`: definitions for most of the functions used in the rest of the notebooks.

- `transition_elastic_constants.ipynb`: effect of different ratios of the elastic constants (Figs. 2b-c).

- `global_order_diagram.ipyn`: global order as a function of the correlation and anchoring lengths (Fig. 2d, Extended Data Fig. 1a).

- `transition_parameters.ipynb`: creation of topological defects out of a uniform field by increasing the anchoring (Fig. 2e, Extended Data Fig. 1b).

- `transition_geometry.ipynb`: effect of geometry in type, number and position of topological defects (Fig. 3, Extended Data Fig. 2).

- `microlumina_data_analysis.ipynb`: fitting of the shape model to epiblast boundary segmentations, comparison of microlumina and topological defect positions (Fig. 4, Extended Data Fig. 3).

- `3D.ipynb`: comparison of elastic energy density formulations, verification of axial symmetry.

## License

This project is covered under the [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/) license.
